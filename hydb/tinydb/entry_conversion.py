from datetime import timedelta
from functools import singledispatchmethod
from pathlib import Path, PosixPath
from typing import Any, Union

from hytools.logger import Logger
from numpy import ndarray

from ..database import DatabaseBase


class EntryConversion:
    """Class for converting entries to and from dictionaries."""

    @singledispatchmethod
    def obj_to_dict(self, obj: Any) -> Union[dict, Any]:
        """Convert object to dict recursively."""
        err_msg = 'non-representable type: '
        if hasattr(obj, 'as_dict'):
            return self.obj_to_dict(obj.as_dict())
        try:
            d = obj.__dict__
            class_name = obj.__class__.__name__
            module_name = obj.__module__
        except AttributeError:
            try:
                return str(obj)
            except Exception:
                msg = f'{err_msg} {type(obj)}'
                return msg
        else:
            purge = ['FileHandler', 'StreamHandler', 'Logger', 'LoggerDummy']
            if class_name in purge:
                return None
            d.update({'__class__': class_name, '__module__': module_name})
            return {k: self.obj_to_dict(v) for k, v in d.items()}
            # d.update({'__class__': class_name, '__module__': module_name})
            # # try to generate obj from dict, if output is a dict, return None
            # _obj = self.dict_to_obj(d)
            # if isinstance(_obj, dict):
            #     return err_msg
            # return self.obj_to_dict(d)

    @obj_to_dict.register(dict)
    def _(self, obj: dict) -> dict:
        """Convert dict to dict recursively."""
        default_types = (str, int, float, type(None))
        if any([not isinstance(v, default_types) for v in obj.values()]):
            # ll = [type(v) for v in obj.values() if not isinstance(
            #     v, default_types)]
            # self.logger.debug(f'obj_to_dict: dict: {ll}')
            return {k: self.obj_to_dict(v) for k, v in obj.items()}
        else:
            return obj

    @obj_to_dict.register(list)
    @obj_to_dict.register(tuple)
    def _(self, obj: list) -> list:
        """Convert list to dict recursively."""
        return [self.obj_to_dict(v) for v in obj]

    @obj_to_dict.register(ndarray)
    def _(self, obj: ndarray) -> list:
        return obj.tolist()

    @obj_to_dict.register(str)
    @obj_to_dict.register(int)
    @obj_to_dict.register(float)
    @obj_to_dict.register(type(None))
    def _(self, obj: Union[str, int, float, None]
          ) -> Union[str, int, float, None]:
        return obj

    @obj_to_dict.register(Path)
    @obj_to_dict.register(PosixPath)
    def _(self, obj: Union[Path, PosixPath]) -> str:
        return str(obj)

    @obj_to_dict.register(timedelta)
    def _(self, obj: timedelta) -> dict:
        keys = ['days', 'microseconds', 'seconds']
        d = {k: getattr(obj, k) for k in keys}
        d.update({'__class__': 'timedelta'})
        d.update({'__module__': 'datetime'})
        return d

    @obj_to_dict.register(DatabaseBase)
    @obj_to_dict.register(Logger)
    def _(self, obj: Union[DatabaseBase, Logger]) -> dict:
        return {}

    def dict_to_obj_recurr(self, dictionary: dict) -> Union[dict, Any]:
        """Resolve objs in dict recursively."""
        if not isinstance(dictionary, dict):
            return dictionary
        for k, v in dictionary.items():
            if isinstance(v, dict):
                dictionary[k] = self.dict_to_obj(v)
                if '__class__' in v:
                    dictionary[k] = self._to_obj(v)
            elif isinstance(v, (list, tuple)):
                dictionary[k] = [self.dict_to_obj(vv) for vv in v]
        if '__class__' in dictionary:
            return self._to_obj(dictionary)
        return dictionary

    def dict_to_obj(self, dictionary: dict) -> Union[dict, Any]:
        """Resolve objs in dict recursively."""
        if not isinstance(dictionary, dict):
            return dictionary
        dictionary.pop('db_id', None)
        obj = self.dict_to_obj_recurr(dictionary)
        # if isinstance(obj, dict):
        #     obj.update({'db_id': db_id})
        # else:
        #     with suppress(AttributeError):
        #         obj.db_id = db_id
        return obj

    def _to_obj(self, dictionary: dict) -> Union[dict, Any]:
        """Convert dict to object."""
        class_name = dictionary.pop('__class__')
        module_name = dictionary.pop('__module__')
        try:
            module = __import__(module_name)
            class_ = getattr(module, class_name)
            try:
                instance = class_(**dictionary)
            except (AttributeError, ValueError, TypeError):
                instance = class_()
                for k, v in dictionary.items():
                    setattr(instance, k, v)
            # self.logger.debug(f'_to_obj: (output) {instance}')
            return instance
        except Exception:
            # self.logger.debug(f'Could not convert dictionary to object: {e}')
            dictionary.update({'__class__': class_name,
                               '__module__': module_name})
            return dictionary

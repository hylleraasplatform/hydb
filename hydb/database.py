from abc import abstractmethod
from typing import Any, Dict, List, Optional, Union

from hytools.connection import Connection

# Connection inherits from ABC, so it is an abstract base class.


class DatabaseBase(Connection):
    """Abstract base class for database operations."""

    @abstractmethod
    def __hash__(self) -> int:
        """Hash the database object."""
        pass

    @abstractmethod
    def __eq__(self, other: Any) -> bool:
        """Check if two database objects are equal."""
        pass

    @abstractmethod
    def __ne__(self, other: Any) -> bool:
        """Check if two database objects are not equal."""
        pass

    @abstractmethod
    def insert_one(self, entry: Any) -> int:
        """Insert an entry into the database."""
        pass

    @abstractmethod
    def insert_many(self, entries: List[Any]) -> List[int]:
        """Insert multiple entries into the database."""
        pass

    @abstractmethod
    def search_one(self, *args, **kwargs) -> Optional[Dict[str, Any]]:
        """Search the database."""
        pass

    @abstractmethod
    def search_many(self, *args, **kwargs) -> List[Optional[Dict[str, Any]]]:
        """Search the database."""
        pass

    @abstractmethod
    def update_one(self, entry: Any, *args, **kwargs) -> int:
        """Update an entry in the database."""
        pass

    @abstractmethod
    def update_many(self, entries: List[Any], *args) -> List[int]:
        """Update multiple entries in the database."""
        pass

    @abstractmethod
    def delete_one(self, *args, **kwargs) -> None:
        """Delete an entry from the database."""
        pass

    @abstractmethod
    def delete_many(self, *args, **kwargs) -> None:
        """Delete multiple entries from the database."""
        pass

    @abstractmethod
    def fetch_all(self, key: Any) -> List:
        """Fetch all entries from the database."""
        pass

    @abstractmethod
    def resolve(self, entry: Union[List[Dict[str, Any]], Dict[str, Any]]
                ) -> Union[List[Any], Any]:
        """Resolve a dictionary of entries."""
        pass

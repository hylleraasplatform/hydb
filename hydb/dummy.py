from typing import Any, Dict, List, Optional, Union

from hytools.connection import ConnectionDummy

from .database import DatabaseBase


class DatabaseDummy(DatabaseBase, ConnectionDummy):
    """Dummy implementation of the Database abstract base class."""

    def __hash__(self) -> int:
        """Hash the database object."""
        return 0

    def __eq__(self, other: Any) -> bool:
        """Check if two database objects are equal."""
        return False

    def __ne__(self, other: Any) -> bool:
        """Check if two database objects are not equal."""
        return False

    def insert_one(self, entry: Any) -> int:
        """Insert an entry into the database."""
        return 0

    def insert_many(self, entries: List[Any]) -> List[int]:
        """Insert multiple entries into the database."""
        return []

    def search_one(self, *args, **kwargs) -> Optional[Dict[str, Any]]:
        """Search the database."""
        return {}

    def search_many(self, *args, **kwargs) -> List[Optional[Dict[str, Any]]]:
        """Search the database."""
        return []

    def update_one(self, entry: Any, *args, **kwargs) -> int:
        """Update an entry in the database."""
        return 0

    def update_many(self, entries: List[Any], *args, **kwargs) -> List[int]:
        """Update multiple entries in the database."""
        return []

    def delete_one(self, *args, **kwargs) -> None:
        """Delete an entry from the database."""
        pass

    def delete_many(self, *args, **kwargs) -> None:
        """Delete multiple entries from the database."""
        pass

    def fetch_all(self, key: Any) -> List:
        """Fetch all entries from the database."""
        return []

    def resolve(self, entry: Union[List[Dict[str, Any]], Dict[str, Any]]
                ) -> Union[List[Any], Any]:
        """Resolve a dictionary of entries."""
        return entry

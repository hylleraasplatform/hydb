import unittest
from datetime import timedelta
from pathlib import Path

from numpy import array

from hydb.tinydb.entry_conversion import EntryConversion


class TestEntryConversion(unittest.TestCase):
    """Test the EntryConversion class."""

    def setUp(self):
        """Set up the test."""
        self.converter = EntryConversion()

    def test_obj_to_dict_with_dict(self):
        """Test the obj_to_dict method with a dictionary."""
        obj = {'key1': 'value1', 'key2': 2, 'key3': 3.0}
        expected = {'key1': 'value1', 'key2': 2, 'key3': 3.0}
        result = self.converter.obj_to_dict(obj)
        self.assertEqual(result, expected)

    def test_obj_to_dict_with_list(self):
        """Test the obj_to_dict method with a list."""
        obj = ['value1', 2, 3.0]
        expected = ['value1', 2, 3.0]
        result = self.converter.obj_to_dict(obj)
        self.assertEqual(result, expected)

    def test_obj_to_dict_with_ndarray(self):
        """Test the obj_to_dict method with a numpy array."""
        obj = array([1, 2, 3])
        expected = [1, 2, 3]
        result = self.converter.obj_to_dict(obj)
        self.assertEqual(result, expected)

    def test_obj_to_dict_with_path(self):
        """Test the obj_to_dict method with a Path object."""
        obj = Path('/some/path')
        expected = str(obj)
        result = self.converter.obj_to_dict(obj)
        self.assertEqual(result, expected)

    def test_obj_to_dict_with_timedelta(self):
        """Test the obj_to_dict method with a timedelta object."""
        obj = timedelta(days=1, seconds=3600, microseconds=1000)
        expected = {
            'days': 1,
            'seconds': 3600,
            'microseconds': 1000,
            '__class__': 'timedelta',
            '__module__': 'datetime'
        }
        result = self.converter.obj_to_dict(obj)
        self.assertEqual(result, expected)

    def test_dict_to_obj(self):
        """Test the dict_to_obj method."""
        dictionary = {
            '__class__': 'timedelta',
            '__module__': 'datetime',
            'days': 1,
            'seconds': 3600,
            'microseconds': 1000
        }
        expected = timedelta(days=1, seconds=3600, microseconds=1000)
        result = self.converter.dict_to_obj(dictionary)
        self.assertEqual(result, expected)

    def test_dict_to_obj_with_non_dict(self):
        """Test the dict_to_obj method with a non-dictionary."""
        obj = 'string'
        result = self.converter.dict_to_obj(obj)
        self.assertEqual(result, obj)

    def test_dict_to_obj_recurr(self):
        """Test the dict_to_obj_recurr method."""
        dictionary = {
            'key1': {
                '__class__': 'timedelta',
                '__module__': 'datetime',
                'days': 1,
                'seconds': 3600,
                'microseconds': 1000
            },
            'key2': 'value2'
        }
        expected = {
            'key1': timedelta(days=1, seconds=3600, microseconds=1000),
            'key2': 'value2'
        }
        result = self.converter.dict_to_obj_recurr(dictionary)
        self.assertEqual(result, expected)

    def test_to_obj(self):
        """Test the _to_obj method."""
        dictionary = {
            '__class__': 'timedelta',
            '__module__': 'datetime',
            'days': 1,
            'seconds': 3600,
            'microseconds': 1000
        }
        expected = timedelta(days=1, seconds=3600, microseconds=1000)
        result = self.converter._to_obj(dictionary)
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

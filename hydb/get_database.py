from typing import Optional

from .database import DatabaseBase
from .dummy import DatabaseDummy
from .tinydb import DatabasePython


def get_database(*args, db_type: Optional[str] = 'python', **kwargs
                 ) -> DatabaseBase:
    """Return a database object."""
    database_classes = {
        'dummy': DatabaseDummy,
        'python': DatabasePython,
        'tinydb': DatabasePython,
        }
    if args and isinstance(args[0], DatabaseBase):
        return args[0]

    try:
        return database_classes[db_type](*args, **kwargs)
    except KeyError:
        raise ValueError('Database type not recognized')

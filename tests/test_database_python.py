
from __future__ import annotations

import json
import pathlib
import shutil
from dataclasses import dataclass, field
# from datetime import datetime, timedelta
from typing import Any, Tuple

import mock

from hydb import get_database

# from hytools.logger import get_logger


@dataclass
class A:
    """Test class."""

    a: int = 1
    b: str = '2'
    c: list = field(default_factory=lambda: [3])


@dataclass
class B:
    """Test class."""

    d: int = 4
    e: int = 5
    f: Any = None


def _test_paths() -> Tuple[pathlib.Path]:
    """Test the paths."""
    home_path: pathlib.Path = pathlib.Path.home()
    hylleraas_path: pathlib.Path = home_path / '.hylleraas'
    database_path: pathlib.Path = hylleraas_path / 'calculations.json'
    return home_path, hylleraas_path, database_path  # type: ignore


def test_database_no_hylleraas_directory(tmp_path: pathlib.Path) -> None:
    """Test the database."""
    with mock.patch('pathlib.Path.home', return_value=tmp_path):
        try:
            get_database(db_type='python')
        except Exception:
            test_str: str = (
                'Database error trying to setup a new database on a system '
                'with no pre-existing ~/.hylleraas.'
            )
            assert False, test_str
    shutil.rmtree(tmp_path)


def test_database_empty_hylleraas_directory(tmp_path: pathlib.Path) -> None:
    """Test the database."""
    with mock.patch('pathlib.Path.home', return_value=tmp_path):
        _, hylleraas_path, _ = _test_paths()  # type: ignore
        hylleraas_path.mkdir(exist_ok=False, parents=False)

        try:
            get_database(db_type='python')
        except Exception:
            test_str: str = (
                'Database error trying to setup a new database on a system '
                'with pre-existing, empty ~/.hylleraas.'
            )
            assert False, test_str
    shutil.rmtree(tmp_path)


def test_database_empty_database_file(tmp_path: pathlib.Path) -> None:
    """Test the database."""
    with mock.patch('pathlib.Path.home', return_value=tmp_path):
        _, hylleraas_path, hylleraas_database_path = \
                                    _test_paths()  # type: ignore
        hylleraas_path.mkdir(exist_ok=False, parents=False)
        hylleraas_database_path.touch(exist_ok=False)

        try:
            get_database(db_type='python')
        except Exception:
            test_str: str = (
                'Database error trying to setup a new database on a system '
                'with pre-existing ~/.hylleraas containing an empty 0-byte '
                'file called hylleraas.db.'
            )
            assert False, test_str
    shutil.rmtree(tmp_path)


# def test_database_hylleraas_database_exists_and_is_valid(
#     tmp_path: pathlib.Path
# ) -> None:
#     with mock.patch("pathlib.Path.home", return_value=tmp_path):
#         _, hylleraas_path, hylleraas_database_path = _test_paths()
#         hylleraas_path.mkdir(exist_ok=False, parents=False)
#         db: tinydb.database.TinyDB = tinydb.TinyDB(hylleraas_database_path)
#         db.insert_multiple(
#             [
#                 {"id": 0, "date": str(datetime.now())},
#                 {"id": 1, "date": str(datetime.now() + timedelta(-732, 3))},
#                 {"id": 2, "date": str(datetime.now() + timedelta(2, -9))},
#             ]
#         )
#         db.close()

#         try:
#             database: Database = Database()
#             assert database._database.search(tinydb.Query().id.exists())
#            id1: List[Dict[str, Union[str, int]]] =
# database._database.search(
#                 tinydb.Query().id == 1
#             )
#             id1_time: datetime = datetime.strptime(
#                 id1[0]["date"],
#                 "%Y-%m-%d %H:%M:%S.%f"
#             )
#             assert id1_time.year < datetime.now().year

#         except (json.decoder.JSONDecodeError, IsADirectoryError):
#             test_str: str = (
#                 "Database error trying to connect to an existing, valid "
#                 "database located in ~/.hylleraas/hylleraas.db"
#             )
#             assert False, test_str
#     shutil.rmtree(tmp_path)


# # def test_database_specify_path(tmp_path: pathlib.Path) -> None:
# #     with mock.patch("pathlib.Path.home", return_value=tmp_path):
# #         home_path, _, _ = _test_paths()
# #         test_db_path: pathlib.Path = home_path / "test_db.json"

# #         db: tinydb.database.TinyDB = tinydb.TinyDB(test_db_path)
# #         db.insert_multiple(
# #             [
# #                 {"id": 3, "date": str(datetime.now() +
# # timedelta(-3.6e4, 0))},
# #                 {"id": 4, "date": str(datetime.now() +
# # timedelta(+3.6e5, 0))},
# #                 {"id": 5, "date": str(datetime.now())},
# #             ]
# #         )
# #         db.close()

# #         try:
# #             database: Database = Database(database_path=test_db_path)
# #             assert database._database.search(tinydb.Query().id.exists())
# #            id3: List[Dict[str, Union[str, int]]] =
# database._database.search(
# #                 tinydb.Query().id == 3
# #             )
# #             id3_time: datetime = datetime.strptime(
# #                 id3[0]["date"],
# #                 "%Y-%m-%d %H:%M:%S.%f"
# #             )
# #             assert id3_time.year < 1950
# #         except (json.decoder.JSONDecodeError, IsADirectoryError):
# #             test_str: str = (
# #                 "Database error trying to connect to a specified existing,"
# #                 "valid database located in ~/test_db.json"
# #             )
# #             assert False, test_str
# #     shutil.rmtree(tmp_path)


def test_database_specify_path_empty(tmp_path: pathlib.Path) -> None:
    """Test the database."""
    with mock.patch('pathlib.Path.home', return_value=tmp_path):
        home_path, _, _ = _test_paths()  # type: ignore
        test_db_path: pathlib.Path = home_path / 'test_db.json'

        try:
            get_database(db_type='python', database_path=test_db_path)
        except (json.decoder.JSONDecodeError, IsADirectoryError):
            test_str: str = (
                'Database error trying to connect to a specified '
                'non-existant file located in ~/test_db.json'
            )
            assert False, test_str
    shutil.rmtree(tmp_path)


# #     db.add(job)
# #     db.add(job)
# #     db.add(job)
# #     print('POST\n:', db._database.all())
# #     entry = db.get('id', 1)
# #     print(db.resolve('id', 65))
# #     print('\ndb1')

# def test_database_init():
#     """Test the database initialization."""
#     db0 = get_database('running.json')
#     assert db0.database_name == 'running'
#     db1 = get_database('~/.hylleraas/running.json')
#     assert db1 == db0
#     db1 = get_database('running')
#     assert db1 == db0
#     db1 = get_database('running', database_path='~/.hylleraas/')
#     assert db1 == db0
#     db1 = get_database(db0)
#     assert db1 == db0


# def test_conversion():
#     """Test the to_obj method."""
#     db = get_database('running.json')
#     obj = B(f=A())
#     dict = db.obj_to_dict(obj)
#     assert dict['f']['a'] == 1
#     assert '__class__' in dict['f']
#     obj1 = db.dict_to_obj(dict)
#     assert obj1.f.a == 1


# def test_database_interaction(tmp_path: pathlib.Path):
#     """Test the database interaction."""
#     with mock.patch('pathlib.Path.home', return_value=tmp_path):
#         home_path, _, _ = _test_paths()  # type: ignore
#         test_db_path: pathlib.Path = home_path / 'test_db.json'
#         db = get_database(test_db_path, print_level='debug')
#         job = B(f=A())
#         db.add(job)
#         entry = db.get('id', 0)  # type: ignore
#         assert entry['f']['a'] == 1
#         job2 = db.resolve('id', 0)[0]
#         assert job2.f.a == 1
#         assert len(db.get_all()) == 1
#         db.remove('id', 0)
#         assert len(db.get_all()) == 0

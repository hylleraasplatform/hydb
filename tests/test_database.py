
from __future__ import annotations

# import json
import pathlib
# import shutil
from dataclasses import dataclass, field
from typing import Any, Tuple

import mock

from hydb import get_database

# from hytools.logger import get_logger


# from datetime import datetime, timedelta
# from typing import Any, Dict, List, Tuple, Union


@dataclass
class A:
    """Test class."""

    a: int = 1
    b: str = '2'
    c: list = field(default_factory=lambda: [3])


@dataclass
class B:
    """Test class."""

    d: int = 4
    e: int = 5
    f: Any = None


def _test_paths() -> Tuple[pathlib.Path]:
    """Test the paths."""
    home_path: pathlib.Path = pathlib.Path.home()
    hylleraas_path: pathlib.Path = home_path / '.hylleraas'
    database_path: pathlib.Path = hylleraas_path / 'calculations.json'
    return home_path, hylleraas_path, database_path  # type: ignore


# def test_integration(tmp_path: pathlib.Path):
#     """Test the database integration."""
#     from hyif import Xtb
#     from hyobj import Molecule

#     myxtb = Xtb({'check_version': False})
#     mymol = Molecule('O')
#     logger = get_logger(print_level='error')

#     with mock.patch('pathlib.Path.home', return_value=tmp_path):
#         home_path, _, _ = _test_paths()  # type: ignore
#         test_db_path: pathlib.Path = home_path / 'test_db.json'

#         db = get_database(test_db_path, logger=logger)
#         db.open()
#         job = B(f=myxtb.setup(mymol))
#         dict = db.obj_to_dict(job)  # type: ignore
#         assert dict['f']['program'] == 'xtb'
#         id = db.insert_one(job)
#         assert db.contains(doc_id=id)  # type: ignore
#         ids = db.insert_many([job, job, job])
#         assert len(db.db.all()) == 4  # type: ignore
#         entries = db.fetch_all(key='f')
#         assert len(entries) == 4
#         entry = db.search_one(id=id, resolve=True)
#         assert entry.d == 4  # type: ignore
#         entries = db.search_many(ids=ids)
#         assert entries[0].doc_id == ids[0]  # type: ignore
#         db.update_one({'B': 5}, id)
#         entry = db.search_one(id=id)
#         assert entry['B'] == 5
#         db.delete_one(id=id)
#         assert len(db.db.all()) == 3  # type: ignore
#         db.update_many([{'B': 5}, {'B': 6}, {'B': 7}], ids)
#         entries = db.search_many(ids=ids)
#         assert entries[0]['B'] == 5
#         db.delete_many(ids)
#         assert len(db.db.all()) == 0  # type: ignore

def test_integration(tmp_path: pathlib.Path):
    """Test the database integration."""
    myxtb = {'program': 'xtb'}

    with mock.patch('pathlib.Path.home', return_value=tmp_path):
        home_path, _, _ = _test_paths()  # type: ignore
        test_db_path: pathlib.Path = home_path / 'test_db.json'

        db = get_database(test_db_path)
        db.open()
        job = B(f=myxtb)
        dict = db.obj_to_dict(job)  # type: ignore
        assert dict['f']['program'] == 'xtb'
        id = db.insert_one(job)
        assert db.contains(doc_id=id)  # type: ignore
        ids = db.insert_many([job, job, job])
        assert len(db.db.all()) == 4  # type: ignore
        entries = db.fetch_all(key='f')
        assert len(entries) == 4
        entry = db.search_one(id=id, resolve=True)
        assert entry.d == 4  # type: ignore
        entries = db.search_many(ids=ids)
        assert entries[0].doc_id == ids[0]  # type: ignore
        db.update_one({'B': 5}, id)
        entry = db.search_one(id=id)
        assert entry['B'] == 5
        db.delete_one(id=id)
        assert len(db.db.all()) == 3  # type: ignore
        db.update_many([{'B': 5}, {'B': 6}, {'B': 7}], ids)
        entries = db.search_many(ids=ids)
        assert entries[0]['B'] == 5
        db.delete_many(ids)
        assert len(db.db.all()) == 0

from __future__ import annotations

import hashlib
import os
import pathlib
# from contextlib import suppress
# from datetime import timedelta
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Dict, Union

# import operator
import tinydb
from hytools.connection import ConnectionDummy
from hytools.logger import Logger, LoggerDummy

from ..database import DatabaseBase
from .interactions import TinyDBInteractions
from .tinydb_custom import PickleStorage, YAMLStorage

STORAGE_MAP = {'json': tinydb.storages.JSONStorage, 'yaml': YAMLStorage,
               'pickle': PickleStorage}


class DatabasePython(TinyDBInteractions, DatabaseBase, ConnectionDummy):
    """Database class for Hylleraas calculations."""

    def __init__(
            self,
            *args,
            database_path: Union[str, pathlib.PurePath, os.PathLike] = None,
            database_name: str = 'calculations',
            storage_type: str = 'json',
            logger: Logger = None,
            **kwargs
            ) -> None:
        """Construct the Hylleraas database class.

        Initiates an TinyDB json database populated by simulation data
        (planned, ongoing, or finished) for each simulation initiated through
        the Hylleraas software platform. The default location of this database
        file is:

            ~/.hylleraas/calculations.json

        Parameters
        ----------
        *args : Union[str, pathlib.PurePath, os.PathLike, Database]
            path to database file, by default None
        database_path : str or pathlib.PurePath or os.PathLike
            Connect the Hylleraas software platform to the specified
            non-default database located at `database_path`.
        logger : Logger, optional
            Logger object, by default LoggerDummy()
        storage_type : str, optional
            Storage type of the database, by default 'json'
        database_name : str, optional
            Name of the database, by default 'calculations'

        Raises
        ------
        FileExistsError
            If the path ~/.hylleraas exists, but is a file and not a directory.
        DATABASE ERROR
            If the ~/.hylleraas/hylleraas.db file exists, but is not a
            valid ..............
            ..............

        """
        self.logger = logger or LoggerDummy()

        self.database_path = database_path
        self.database_name = database_name
        self.name = database_name
        self.storage_type = (storage_type or
                             self._get_storage_from_name(database_name))

        if len(args) > 1:
            raise TypeError(
                'Too many positional arguments, only one is allowed: '
                'database_path or database_name'
                )
        elif len(args) == 1:
            self._get_var_from_arg(args[0])

        if self.storage_type not in STORAGE_MAP:
            raise ValueError(
                f'Unknown storage type {storage_type}, '
                f'choose one of {list(STORAGE_MAP.keys())}'
                )
        self.logger.debug('Parsed database_path: ' + str(self.database_path))
        self.logger.debug('Parsed database_name: ' + self.database_name)
        self.logger.debug('Parsed storage_type: ' + self.storage_type)

        if self.database_path is None:
            home_path: pathlib.PurePath = pathlib.Path.home()
            hylleraas_path: pathlib.PurePath = home_path / '.hylleraas'
            try:
                hylleraas_path.mkdir(parents=False,  # type: ignore
                                     exist_ok=True)
            except FileExistsError as e:
                error0: Exception = NotADirectoryError(
                    f'The path {str(hylleraas_path)} exists, but is not a '
                    f'directory. Cannot resolve default database path, '
                    f'specify an explicit database path using '
                    f'hylleraas.DataBasePython(database_path=...).'
                    )
                raise error0 from e
            else:
                file = self.database_name + '.' + self.storage_type
                self.database_path = hylleraas_path / file
        else:
            self.database_path = pathlib.Path(self.database_path)
            if self.database_path.is_dir():
                file = self.database_name + '.' + self.storage_type
                self.database_path = self.database_path / file
        try:
            self.database_path.touch(exist_ok=True)  # type: ignore
        except AttributeError:
            pass

        self.logger.debug(f'Using database at {str(self.database_path)}')

        try:
            self.db: tinydb.database.Database = tinydb.TinyDB(
                self.database_path, storage=STORAGE_MAP[self.storage_type]
                )
        except Exception as e:
            error1: Exception = Exception(
                'Could not connect to the database at specified path: '
                f'{str(self.database_path)}, it is not a valid database '
                f'file for storage type {STORAGE_MAP[self.storage_type]}.'
                )
            raise error1 from e

    def __hash__(self) -> int:
        """Return the hash of the database."""
        hash_input = f'{self.database_path}'
        return int(hashlib.sha256(hash_input.encode()).hexdigest(), 16)

    def __copy__(self) -> DatabasePython:
        """Return a copy of the database."""
        return DatabasePython(database_path=self.database_path,
                              database_name=self.database_name,
                              storage_type=self.storage_type,
                              logger=self.logger)

    def __ne__(self, other: Any) -> bool:
        """Check if two databases are not equal."""
        return not self.__eq__(other)

    def open(self) -> None:
        """Open the database."""
        if not self.db._opened:
            self.db = tinydb.TinyDB(self.database_path,
                                    storage=STORAGE_MAP[self.storage_type])

    def close(self) -> None:
        """Close the database."""
        self.db.close()

    def remove(self) -> None:
        """Delete the database."""
        self.close()
        Path(self.database_path).unlink()

    def __deepcopy__(self, memo: dict) -> DatabasePython:
        """Deepcopy the database."""
        # cls = self.__class__
        # result = cls.__new__(cls)
        # memo[db_id(self)] = result
        # for k, v in self.__dict__.items():
        #     setattr(result, k, deepcopy(v, memo))
        # return result
        return DatabasePython(database_path=self.database_path,
                              database_name=self.database_name,
                              storage_type=self.storage_type,
                              logger=self.logger)

    def __repr__(self) -> str:
        """Return a string representation of the database."""
        return f'DatabasePython(database_path={self.database_path}, ' \
               f'database_name={self.database_name}, ' \
               f'storage_type={self.storage_type})'

    def __str__(self) -> str:
        """Return a string representation of the database."""
        return f'DatabasePython(database_path={self.database_path}, ' \
               f'database_name={self.database_name}, ' \
               f'storage_type={self.storage_type})'

    def __eq__(self, other: Any) -> bool:
        """Check if two databases are equal."""
        if isinstance(other, DatabasePython):
            return (self.database_path == other.database_path and
                    self.database_name == other.database_name and
                    self.storage_type == other.storage_type)
        else:
            return False

    def __getitem__(self, id: int) -> Dict[str, Any]:
        """Get an item from the database."""
        return self.db.get(doc_id=id)
        # db_id = self._db_id(db_id)
        # entry = self.get('db_id', db_id)
        # self.logger.debug(f' __getitem__: {db_id}, {entry}')
        # if not isinstance(entry, list):
        #     entry = [entry]
        # if len(entry) == 0:
        #     raise KeyError(f'No entry with db_id {db_id} in database')
        # elif len(entry) > 1:
        #     raise KeyError(f'More than one entry with db_id {db_id} ' +
        #                    'in database')
        # return entry[0]

    def __setitem__(self, id: int, entry: Any) -> None:
        """Set an item in the database."""
        self.update_one(entry, id=id)

    def __iter__(self) -> Any:
        """Iterate over the database."""
        return iter(self.db)

    def __len__(self) -> int:
        """Return the number of entries in the database."""
        return len(self.db)

    def __in__(self, id: int) -> bool:
        """Check if an item is in the database."""
        return self.db.contains(doc_id=id)

    @singledispatchmethod
    def _get_var_from_arg(self, arg: Any) -> None:
        """Get the database path from the command line argument."""
        if isinstance(arg, DatabasePython):
            self.database_path = arg.database_path
            self.database_name = arg.database_name
            self.storage_type = arg.storage_type
        else:
            raise TypeError('Argument must be a Database, string or ' +
                            f'pathlib.Path, got {type(arg)}')

    @_get_var_from_arg.register(str)
    @_get_var_from_arg.register(os.PathLike)
    @_get_var_from_arg.register(pathlib.Path)
    def _(self, arg: Union[str, pathlib.PurePath, os.PathLike]) -> None:
        """Get the database path from the command line argument."""
        if '~' in str(arg):
            path = pathlib.Path(arg).expanduser()
        else:
            path = pathlib.Path(arg)

        if path.is_dir():
            self.database_path = path
            self.database_name = 'calculations'
        else:
            self.database_name = path.stem
            self.storage_type = self._get_storage_from_name(self.database_name)
            self.database_path = path.parent
            if (self.database_path == pathlib.Path('.') and
                    './' not in str(arg)):
                self.database_path = None

    @_get_var_from_arg.register(DatabaseBase)
    def _(self, arg: DatabaseBase) -> None:
        """Get the database path from the command line argument."""

    @singledispatchmethod
    def _get_storage_from_name(self,
                               name: Union[str, pathlib.Path, os.PathLike]
                               ) -> str:
        """Get the storage type from the database name."""
        return 'json'

    @_get_storage_from_name.register(str)
    def _(self,  # type: ignore[override]
          name: str) -> str:
        """Get the storage type from the database name."""
        storage_type = 'json'
        if '.' in name:
            path = pathlib.Path(name)
            name = path.stem
            if path.suffix != '':
                storage_type = path.suffix[1:]
        return storage_type

    @_get_storage_from_name.register(os.PathLike)
    @_get_storage_from_name.register(pathlib.Path)
    def _(self,  # type: ignore[override]
          name: Union[pathlib.Path, os.PathLike]) -> str:
        """Get the storage type from the database name."""
        path = pathlib.Path(name)
        name = path.stem
        if path.suffix != '':
            storage_type = path.suffix[1:]
        else:
            storage_type = 'json'
        return storage_type

    # def generate_unique_db_identifier(self) -> int:
    #     """Generate a new and unique calculation db_identifier integer.
    #
    #     Returns
    #     -------
    #     unique_db_id : int
    #         A new and unique calculation db_identifier integer
    #
    #     """
    #     largest_db_id: int = -1
    #     if len(self.db) > 0:
    #         for item in self.db.all():
    #             if item.get('db_id', -1) > largest_db_id:
    #                 largest_db_id += 1
    #     return largest_db_id + 1

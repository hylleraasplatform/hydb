from functools import singledispatchmethod
from typing import Any, Dict, List, Optional, Union

import tinydb

from .entry_conversion import EntryConversion
from .queries import TinyDBQueries


class TinyDBInteractions(TinyDBQueries, EntryConversion):
    """Class for interacting with TinyDB."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = None
        self.logger = None

    @singledispatchmethod
    def insert(self, entry: Union[Any, list]) -> Union[List[int], int]:
        """Insert an entry into the database."""
        return self.insert_one(entry)

    @insert.register(list)
    def _(self, entry: list) -> List[int]:  # type: ignore
        """Insert multiple entries into the database."""
        return self.insert_many(entry)

    def insert_one(self, entry: Any) -> int:
        """Insert an entry into the database."""
        entry = self.obj_to_dict(entry)
        entry.pop('doc_id', None)
        self.logger.debug(f'insert_one: {entry}')
        return self.db.insert(entry)

    def insert_many(self, entries: List[Any]) -> List[int]:
        """Insert multiple entries into the database."""
        entries = [self.obj_to_dict(entry) for entry in entries]
        for entry in entries:
            entry.pop('doc_id', None)
        self.logger.debug(f'insert_many: {entries}')
        return self.db.insert_multiple(entries)

    def search_one(self, *args, **kwargs
                   ) -> Optional[Union[Dict[str, Any], Any]]:
        """Search the database."""
        query = self.gen_query(*args, **kwargs)
        self.logger.debug(f'search_one: {query}')
        entry = (self.db.get(**query)
                 if isinstance(query, dict) else self.db.get(query))
        self.logger.debug(f'search_one: {entry}')
        if not entry:
            return None
        entry['doc_id'] = entry.doc_id  # type: ignore
        return self.resolve(entry) if kwargs.get('resolve', False) else entry

    def search_many(self, *args, **kwargs) -> List[Union[Dict[str, Any], Any]]:
        """Search the database."""
        if len(args) > 1:
            entries = [self.search_one(arg, **kwargs) for arg in args]
        else:
            query = self.gen_query(*args, **kwargs)
            self.logger.debug(f'search_many: {query}')
            entries = self.db.get(
                **query) if isinstance(query, dict) else self.db.get(query)
        self.logger.debug(f'search_many: {entries}')
        if not entries:
            return []
        for entry in entries:
            entry['doc_id'] = entry.doc_id  # type: ignore
        return [self.resolve(entry) if kwargs.get('resolve', False) else entry
                for entry in entries]

    def update_one(self, entry: Any, *args, **kwargs) -> int:
        """Update an entry in the database."""
        if len(args) != 1:
            raise ValueError('Invalid number of arguments')
        id = args[0]
        id = [id] if not isinstance(id, list) else id
        entry = self.obj_to_dict(entry)
        self.logger.debug(f'update_one: {id}, {entry}')
        return self.db.update(entry, doc_ids=id)

    def update_many(self, entries: List[Any], *args, **kwargs) -> List[int]:
        """Update multiple entries in the database."""
        ids = args[0]
        if not ids or len(entries) != len(ids) or len(args) != 1:
            raise ValueError('Invalid number of arguments')
        entries = [self.obj_to_dict(entry) for entry in entries]
        self.logger.debug(f'update_many: {ids}, {entries}')
        # might be a list of lists
        return [self.db.update(entry, doc_ids=[id])
                for entry, id in zip(entries, ids)]

    def delete_one(self, id: int) -> None:
        """Delete an entry from the database."""
        self.logger.debug(f'delete_one: {id}')
        return self.db.remove(doc_ids=[id])

    def delete_many(self, ids) -> None:
        """Delete multiple entries from the database."""
        self.logger.debug(f'delete_many: {ids}')
        return self.db.remove(doc_ids=ids)

    def resolve(self, entry: Union[List[Dict[str, Any]], Dict[str, Any]]
                ) -> Union[List[Any], Any]:
        """Resolve a dictionary of entries."""
        if isinstance(entry, dict):
            id = entry.pop('doc_id')
        elif isinstance(entry, list):
            id = [e.pop('doc_id') for e in entry]
        self.logger.debug(f'resolve: {id}')
        return self.dict_to_obj(entry) if isinstance(entry, dict) else \
            [self.dict_to_obj(e) for e in entry]

    def fetch_all(self, key: Any) -> List:
        """Fetch all entries from the database."""
        entries = self.db.search(tinydb.Query()[key].exists())
        self.logger.debug(f'fetchall: {key}')
        return [entry[key] for entry in entries if entry[key] is not None]

    def contains(self, *args, **kwargs) -> bool:
        """Check if the database contains an entry."""
        query = self.gen_query(*args, **kwargs)
        self.logger.debug(f'contains: {query}')
        return self.db.contains(
            **query) if isinstance(query, dict) else self.db.contains(query)

import pickle

import yaml  # type: ignore[import-untyped]
from tinydb import Storage


class YAMLStorage(Storage):
    """Storage backend using yaml."""

    def __init__(self, filename):  # (1)
        """Initialize."""
        self.filename = filename

    def read(self):
        """Read data from file."""
        with open(self.filename) as handle:
            try:
                data = yaml.safe_load(handle.read())  # (2)
                return data
            except yaml.YAMLError:
                return None  # (3)

    def write(self, data):
        """Write data to file."""
        with open(self.filename, 'w+') as handle:
            yaml.dump(data, handle)

    def close(self):  # (4)
        """Close file."""
        pass


class PickleStorage(Storage):
    """Storage backend using Python's built-in pickle module.

    This storage backend serializes data using Python's built-in
    :mod:`pickle` module. It is the default storage backend.

    Parameters
    ----------
    filename : str
        Path to the database file.

    """

    def __init__(self, filename):  # (1)
        """Initialize."""
        self.filename = filename
        self.filename.touch(exist_ok=True)
        init = self.read()
        if init is None:
            self.write({})

    def read(self):
        """Read data from file."""
        with open(self.filename, 'rb') as handle:
            try:
                data = pickle.load(handle)  # (2)
                return data
            except pickle.PickleError:
                return None  # (3)
            except EOFError:
                return None

    def write(self, data):
        """Write data to file."""
        with open(self.filename, 'wb+') as handle:
            pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def close(self):  # (4)
        """Close file."""
        pass

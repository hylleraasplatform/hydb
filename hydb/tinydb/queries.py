import operator
from functools import singledispatchmethod
from typing import Any, Dict, Optional, Union

import tinydb

class_hierarchy = ['QueryLike', 'QueryInstance', 'QueryImpl']
for class_name in class_hierarchy:
    if hasattr(tinydb.queries, class_name):
        query_type = getattr(tinydb.queries, class_name)
        break
else:
    query_type = type(tinydb.Query())


class TinyDBQueries:
    """Class for generating TinyDB queries."""

    def keyword_in_kwargs(self, keyword='id', **kwargs
                          ) -> Optional[list[int]]:
        """Get the id from the keyword arguments."""
        return [v for k, v in kwargs.items() if keyword in k.lower()]

    def gen_query(self, *args, **kwargs):
        """Generate a TinyDB query."""
        if args:
            return self.query_from_arg(*args)
        return self.query_from_kwargs(**kwargs)

    def query_from_kwargs(self, **kwargs):
        """Generate a TinyDB query from the keyword arguments."""
        query = kwargs.get('query')
        if query:
            return query

        key = kwargs.get('key')
        value = kwargs.get('value')
        if key and value:
            op = kwargs.get('op', operator.eq)
            if isinstance(op, str):
                op = getattr(operator, op)
            return op(tinydb.Query()[key], value)

        id = self.keyword_in_kwargs(**kwargs)
        if id:
            return self.query_with_id(id)
        return tinydb.Query().fragment(kwargs)

    def query_with_id(self, doc_id: Union[list[int], int]) -> Dict[str, int]:
        """Generate a TinyDB query with the doc_id."""
        if not doc_id:
            raise ValueError('No id provided')
        # Flatten the list if doc_id is a list of lists
        if isinstance(doc_id, list) and any(isinstance(i, list)
                                            for i in doc_id):
            doc_id = [item for sublist in doc_id
                      for item in sublist]  # type: ignore

        return {'doc_ids': doc_id} if len(doc_id) > 1 else {  # type: ignore
            'doc_id': doc_id[0]}  # type: ignore

    @singledispatchmethod
    def query_from_arg(self, arg: Any) -> Dict[str, Any]:
        """Search the database."""
        raise TypeError(f'Invalid arguments: {arg}')

    @query_from_arg.register(int)
    def _(self, arg: int) -> Dict[str, Any]:
        """Search the database."""
        return self.query_from_kwargs(doc_id=arg)

    @query_from_arg.register(list)
    def _(self, arg: int) -> Dict[str, Any]:
        """Search the database."""
        if any(not isinstance(i, int) for i in arg):  # type: ignore
            raise ValueError('Invalid id')
        return self.query_from_kwargs(doc_ids=arg)

    @query_from_arg.register(dict)
    def _(self, arg: dict) -> Dict[str, Any]:
        """Search the database."""
        return self.query_from_kwargs(**arg)

    @query_from_arg.register(query_type)
    def _(self, arg) -> Dict[str, Any]:
        """Search the database."""
        return arg
